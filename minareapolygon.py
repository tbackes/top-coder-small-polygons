import sys
import math
import random
import copy
import os


"""
This code was constructed to solve a Topcoder Marathon Match problem statement ("Small Polygons")
Given a set of Np points construct a set of up to N polygons, such that the total area of all polygons is minimized.
 - where 10 <= Np < 1500
 - where 2 <= N < 20
 - where all polygons must be simple and cannot intersect each other or share any edges or vertices
 - where all points must be included in exactly one polygon
 - where all polygons must be simple (each point is a vertex with only two edges, edges only intersect at vertices)
 - where all polygons cannot intersect any other polygon (no shared vertices/edges -- but a polygon may be wholly
   contained within another polygon.

The challenge also specified that submitted code should return a solution in < 10 seconds, for any set of parameters in t
he specified range. This project was my first attempt to code in python and I did not end up reaching that benchmark
(my code takes ~4 min to run on my mac for large values of Np... particularly if N is small). However, it was a great
project for my purposes, and I had a lot of fun doing it!

The main function that computes the optimal polygonization is the static function
    Polygon.optimalPolygonization(points, N)

    ### inputs:
    points = list of points, each point is expected to a Pnt object (definied below)
    numiterations = # of iterations to run while searching for optimal polygon

    ### outputs:
    poly = ordered list of indices; indices correspond to each point's location in the original input list "points"
           A "minimal" polygon is formed by traversing the points in the specified order.

A second file was used to interface with the offline Top Coder test/visualization tools. See smallpolygons.py.
The second file reads in a list of points, splits it into N partitions based on the y-coordinate of each point. It then
calls Polygon.optimalPolygonization from this file to compute an approximate "minimal" polygon for each partition.
Lastly, it prints out the full list of (N) polygons to be read into the test/visualization tool.

The java test/visualization tool can be run with my code as follows:
java -jar Tester.jar -exec "./smallpolygons.py" -seed <seed> -vis

where the <seed> parameter is an integer seed used to randomly generate the set of points and max number of polygons.
"""

# ------------- class Point ------------------------------
class Pnt(object):
    def __init__(self, x1, y1):
        self.x = x1
        self.y = y1

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __repr__(self):
        return "<%d, %d>" % (self.x, self.y)

    @staticmethod
    def subtr(p1, p2):
        return Pnt(p1.x - p2.x, p1.y - p2.y)

    @staticmethod
    def norm(p):
        return math.sqrt(p.x * p.x + p.y * p.y)

    @staticmethod
    def dot(p1, p2):
        return p1.x * p2.x + p1.y * p2.y

    @staticmethod
    def dist(p1, p2):
        return Pnt.norm(Pnt.subtr(p1, p2))


# ------------- class Edge ------------------------------
class Edge(object):
    """
        This implementation of class 'Edge' assumes availability of a 'Polygon' object.
        The Polygon has a precomputed distance matrix (polygon.d) that can be used to efficiently compute the
        edge's norm.
    """
    def __init__(self, polygon, p1, p2):
        self.POLY = polygon
        self.i1 = p1                    # index of point 1 in polygon.points
        self.i2 = p2                    # index of point 2 in polygon.points
        self.p1 = polygon.points[p1]    # cartesian coordinates of point 1
        self.p2 = polygon.points[p2]    # cartesian coordinates of point 2
        self.vect = Pnt.subtr(self.p2, self.p1)
        self.norm = polygon.d[p1][p2]

    def __neg__(self):
        return Edge(self.POLY, self.i2, self.i1)

    def __eq__(self, other):
        return (Edge.eq(self.POLY.d[self.i1][other.i1], 0) and Edge.eq(self.POLY.d[self.i2][other.i2], 0)) or \
               (Edge.eq(self.POLY.d[self.i1][other.i2], 0) and Edge.eq(self.POLY.d[self.i2][other.i1], 0))

    def __repr__(self):
        return "{%r, %r}" % (self.p1, self.p2)

    # ---------------------------------------------------
    @staticmethod
    def eq(a, b):
        return abs(a-b) < 1e-9

    # ---------------------------------------------------
    def intersect(self, other):
        """do edges "self" and "other" intersect?"""
        if min(self.p1.x, self.p2.x) > max(other.p1.x, other.p2.x): return False
        if max(self.p1.x, self.p2.x) < min(other.p1.x, other.p2.x): return False
        if min(self.p1.y, self.p2.y) > max(other.p1.y, other.p2.y): return False
        if max(self.p1.y, self.p2.y) < min(other.p1.y, other.p2.y): return False

        den = other.vect.y * self.vect.x - other.vect.x * self.vect.y
        num1 = other.vect.x * (self.p1.y - other.p1.y) - other.vect.y * (self.p1.x - other.p1.x)
        num2 = self.vect.x * (self.p1.y - other.p1.y) - self.vect.y * (self.p1.x - other.p1.x)

        # parallel edges
        if den == 0:
            if min(other.dist2(self), self.dist2(other)) > 0:
                return False
            # on the same line - "not intersect" only if one of the vertices is common,
            # and the other doesn't belong to the line
            if ((self.p1 == other.p1 and Edge.eq(self.POLY.d[self.i2][other.i2], self.norm + other.norm)) or
               (self.p1 == other.p2 and Edge.eq(self.POLY.d[self.i2][other.i1], self.norm + other.norm)) or
               (self.p2 == other.p1 and Edge.eq(self.POLY.d[self.i1][other.i2], self.norm + other.norm)) or
               (self.p2 == other.p2 and Edge.eq(self.POLY.d[self.i1][other.i1], self.norm + other.norm))):
                return False
            return True

        # common vertices
        if self.p1 == other.p1 or self.p1 == other.p2 or self.p2 == other.p1 or self.p2 == other.p2:
            return False

        u1 = num1/den
        u2 = num2/den
        if not(0 <= u1 < 1) or not(0 <= u2 < 1):
            return False
        return True

    # ---------------------------------------------------
    def dist(self, p):
        """distance from point p to this edge"""
        pnt = self.POLY.points[p]
        if Pnt.dot(self.vect, Pnt.subtr(pnt, self.p1)) <= 0:
            return self.POLY.d[p][self.i1]			# from p to p1
        if Pnt.dot(self.vect, Pnt.subtr(pnt, self.p2)) >= 0:
            return self.POLY.d[p][self.i2]			# from p to p2
        # distance to the line itself
        return abs(-self.vect.y * pnt.x + self.vect.x * pnt.y + self.p1.x * self.p2.y - self.p1.y * self.p2.x) / self.norm

    # ---------------------------------------------------
    def dist2(self, other):
        """distance from the closest of the endpoints of edge "other" to this edge"""
        return min(self.dist(other.i1), self.dist(other.i2))

    # ---------------------------------------------------
    def intersectPoly(self):
        """Checks if an edge intersects existing edges of the polygon"""
        for k in xrange(len(self.POLY.poly)):
            if self.intersect(Edge(self.POLY, self.POLY.poly[k], self.POLY.poly[k - 1])):
                return True
        return False


# ------------- class Triangle -------------------------
class Triangle(object):
    """
    This implementation of class 'Triangle' assumes availability of a 'Polygon' object.
    The Polygon has a precomputed distance matrix (polygon.d) that can be used for more efficient computations.
    """
    def __init__(self, polygon, p1, p2, p3):
        # save reference to the Polygon object that this triangle was created within
        self.POLY = polygon
        # triangle vertices
        self.p0 = polygon.points[p1]
        self.p1 = polygon.points[p2]
        self.p2 = polygon.points[p3]
        # triangle indices
        self.i0 = p1
        self.i1 = p2
        self.i2 = p3
        # Edges
        self.e0 = Edge(polygon, p1, p2)
        self.e1 = Edge(polygon, p2, p3)
        self.e2 = Edge(polygon, p3, p1)
        # Area
        self.a = self.area()

    def __repr__(self):
        return '{%r, %r, %r}' % (self.p0, self.p1, self.p2)

    # ---------------------------------------------------
    def area(self):
        """Area of the triangle. Returns infinity for a collinear triangle"""
        e = sorted([self.e0, self.e1, self.e2], key=lambda x: x.norm)      # sort by length of edge
        # if 3 points are collinear:
        if Edge.eq(e[2].norm, e[0].norm + e[1].norm):
            return float("inf")
        else:
            s = (self.e0.norm + self.e1.norm + self.e2.norm) / 2.0
            return math.sqrt(abs(s * (s - self.e0.norm) * (s - self.e1.norm) * (s - self.e2.norm)))

    # ---------------------------------------------------
    def list(self):
        """Returns a list of all the polygon indices used to make this triangle"""
        return [self.i0, self.i1, self.i2]

    # ---------------------------------------------------
    def interiorPoint(self, p):
        """Checks whether point p is included inside the specified triangle t"""
        # compute vectors
        v0 = (-self.e2).vect
        v1 = self.e0.vect
        v2 = Edge(self.POLY, self.i0, p).vect
        # compute dot products
        dot00 = Pnt.dot(v0, v0)
        dot01 = Pnt.dot(v0, v1)
        dot02 = Pnt.dot(v0, v2)
        dot11 = Pnt.dot(v1, v1)
        dot12 = Pnt.dot(v1, v2)

        # compute barycentric coordinates
        if dot00 * dot11 - dot01 * dot01 != 0:
            invDenom = 1.0 / (dot00 * dot11 - dot01 * dot01)
            u = (dot11 * dot02 - dot01 * dot12) * invDenom
            v = (dot00 * dot12 - dot01 * dot02) * invDenom
            # check if point is in triangle
            return (u >= 0) and (v >= 0) and (u + v < 1)
        else:
            # if this is a collinear triangle, does the point reside on the interior of the longest edge?
            if self.a == float("inf"):
                e = max(self.e0, self.e1, self.e2, key=lambda x: x.norm)      # longest edge of collinear triangle
                return Triangle(self.POLY, e.i1, e.i2, p).a == float("inf") and e.norm == self.POLY.d[p][e.i2] + self.POLY.d[e.p1][p]                               # does point intersect?
            # if point forms collinear triangle with two of this collinear triangle's vertices, does it reside between those two points?
            else:
                vlist = self.list()  # list of triangle's vertices
                for i in xrange(3):
                    if Triangle(self.POLY, p, vlist[i - 1], vlist[i]).a == float("inf"):    # collinear with triangle edge?
                        return  self.POLY.d[vlist[i - 1]][vlist[i]] == self.POLY.d[p][vlist[i]] + self.POLY.d[vlist[i - 1]][p]
                return False

    # ---------------------------------------------------
    def interiorEmpty(self, points):
        """Loops through all points to check if triangle is empty"""
        p = [pnt for pnt in points if not(pnt in self.list())]
        for pnt in p:
            if self.interiorPoint(pnt):
                return False
        return True

    # ---------------------------------------------------
    def intersectPoly(self):
        """Checks if a triangle intersects existing edges of the polygon"""
        ind = self.POLY.poly.index(self.i1)
        np = len(self.POLY.poly)
        if ind >= np - 1:           # if edge begins at last vertex in poly
            loopind = range(np)
        elif ind == 0:              # if edge begins at first vertex in poly
            loopind = range(ind + 1, np) + [0]
        else:                       # if edge is in interior of poly list
            loopind = range(ind + 1, np) + range(0, ind + 1)
        for k in xrange(len(loopind) - 1):
            # check intersection of edge k..k+1 of polygon with new triangle edges
            e1 = Edge(self.POLY, self.POLY.poly[loopind[k]], self.POLY.poly[loopind[k + 1]])
            if e1.intersect(self.e0) or e1.intersect(self.e2):
                return True
        return False


# ------------- class Polygon  --------------
class Polygon(object):
    # ---------------------------------------------------
    def __init__(self, points):
        """initialize polygon with an unordered list of points"""
        self.points = points                        # original list of points to build polygon from
        self.np = len(points)                       # number of points in polygon
        self.poly = []                              # ordered indices of polygon vertices (references self.points)
        self.used = [0 for i in range(self.np)]     # flag: 1 = in polygon, 0 = unused point
        self.newedges = []                          # list holding newest edges
        self.feasible = []                          # list of tuples (index of feasible point, best edge for this point, triangle)
        self.unused = range(self.np)                # list of indices of points not in poly and not yet feasible
        self.a = -1                                 # polygon's total area
        self.d = [[-1 for i in range(self.np)] for j in range(self.np)]         # initialize matrix of distances between all points
        self.dsort = [[-1 for i in range(self.np)] for j in range(self.np)]     # indices of all neighboring points, sorted by distance (self = d[0], closest neighbor = d[1], etc.)
        self.calculateDistances()                   # calculate distances for all points

    def __repr__(self):
        return 'points: %r\npoly: %r\n' % (self.points, self.poly)

    def __str__(self):
        return 'points: %r\nnp: %d\npoly: %r\nused: %r\nnewedges: %r\nfeasible: %r\nunused: %r\narea: %f' % \
               (self.points, self.np, self.poly, self.used, self.newedges, self.feasible, self.unused, self.a)

    # ---------------------------------------------------
    def calculateDistances(self):
        """Calculate distance matrix for all points; also sorted list of indices for each point, to simplify looking for nearest neighbor"""
        # calculate the full distance matrix
        for i in xrange(self.np):
            self.d[i][i] = 0
            for j in xrange(i + 1, self.np):
                self.d[i][j] = Pnt.dist(self.points[i], self.points[j])
                self.d[j][i] = self.d[i][j]
            # get sorted indices for each point
            self.dsort[i] = sorted(range(self.np), key=self.d[i].__getitem__)

    # ---------------------------------------------------
    def copy(self):
        """Make a copy of the polygon, reinitialize all values"""
        pcopy = Polygon([])
        pcopy.points = self.points
        pcopy.np = self.np
        pcopy.poly = []                              # ordered indices of polygon vertices (references self.points)
        pcopy.used = [0 for i in range(self.np)]     # flag: 1 = in polygon, 0 = unused point
        pcopy.newedges = []                          # list holding newest edges
        pcopy.feasible = []                          # list of tuples (index of feasible point, best edge, triangle)
        pcopy.unused = range(self.np)                # list of indices of points not in poly and not yet feasible
        pcopy.a = -1                                 # polygon's total area
        pcopy.d = self.d                             # copy over distance matrix (already calculated)
        pcopy.dsort = self.dsort                     # copy over rank ordering of nearest neighbors (already calculated)
        return pcopy

    # ---------------------------------------------------
    def randomTriangle(self):
        """RT: the three points are randomly selected"""
        p = range(self.np)
        # select first two points at random, checking for ties
        pi = p.pop(random.randint(0, self.np - 1))
        pj = p.pop(random.randint(0, self.np - 2))
        # select the third point at random, ensuring that the triangle does not contain any other elements from points
        isValid = False
        while not isValid:
            if len(p) == 0:
                return self.randomTriangle()
            if len(p) == 1:
                pk = p.pop(0)
            else:
                pk = p.pop(random.randint(0, len(p) - 1))
            t = Triangle(self, pi, pj, pk)
            isValid = t.a < float("inf") and t.interiorEmpty([ind for ind in range(self.np) if ind not in [pi, pj, pk]])
        # initialize the polygon as the final valid triangle
        self.poly = [pi, pj, pk]
        self.newedges = [t.e0, t.e1, t.e2]
        self.a = t.a        # polygon's area is now equal to the area of this first triangle
        for item in self.poly:
            self.used[item] = 1   # used
            self.unused.remove(item)
        return

    # ---------------------------------------------------
    def randomGreedyTriangle(self):
        """RGT: the first point is randomly selected adn the other two are the two closest neighbors"""
        # select first point at random
        pi = random.randint(0, self.np - 1)
        # select the closest neighbor
        pj = self.dsort[pi][1]
        # select the next closest neighbor, insuring that the resulting triangle is still valid
        isValid = False
        k = 2
        while not isValid:
            pk = self.dsort[pi][k]
            if pk == pi:
                k += 1
                pk = self.dsort[pj][k]
            t = Triangle(self, pi, pj, pk)
            isValid = t.a < float("inf") and t.interiorEmpty([ind for ind in range(self.np) if ind not in [pi, pj, pk]])
            k += 1
        # initialize the polygon as the final valid triangle
        self.poly = [pi, pj, pk]
        self.newedges = [t.e0, t.e1, t.e2]
        self.a = t.a        # polygon's area is now equal to the area of this first triangle
        for item in self.poly:
            self.used[item] = 1   # used
            self.unused.remove(item)
        return

    # ---------------------------------------------------
    def feasiblePoints(self):
        """
        This method returns the list of all points that can be feasibly inserted into the current polygon.
        Each feasible point is paired with the edge it would replace. This edge is chosen such that the resulting
        triangle has minimum area.
        A point p is feasible if:
          - no remaining points lie in the interior of the polygon when this point is added
          - adding this point does not cause the polygon to have self-intersections
        Output: updates self.feasible and self.unused, does not return any values
        """
        nonpoly = [ind for ind in range(self.np) if self.used[ind] < 1 ]
        # copy indices of old replaced edge (except if the polygon is still a triangle)
        if (len(self.poly) > 3):
            told = [self.newedges[0].i1, self.newedges[1].i2]
        else:
            told = []
        kill = []
        add = []
        # loop through each of the polygon's newly added edges
        for edge in self.newedges:
            # loop through all points previously considered feasible (prior to addition of new edges)
            for i, pnt in enumerate(self.feasible):
                # if pnt not in kill:                          # don't evaluate any points that we already know are invalid
                t1 = Triangle(self, pnt[0], edge.i1, edge.i2)  # create triangle with feasible point & new edge
                # if new triangle represents a valid addition
                if (not t1.intersectPoly()) and t1.interiorEmpty(nonpoly):
                    # if old best choice included the replaced edge --> replace
                    if pnt[1].i1 in told and pnt[1].i2 in told:
                        self.feasible[i] = (pnt[0], edge, t1)
                    # if area is smaller than old best choice, or old best choice intersects new edges --> replace:
                    elif t1.a < pnt[2].a or (edge.intersect(pnt[2].e1) or edge.intersect(pnt[2].e2)):
                        self.feasible[i] = (pnt[0], edge, t1)
                # if existing choice is no longer valid (used in prev step or intersects new edges) --> remove from feasible list:
                elif (pnt[1].i1 in told and pnt[1].i2 in told) or edge.intersect(pnt[2].e1) or edge.intersect(pnt[2].e2):
                    kill.append(pnt)

            # loop through all unused, previously non-feasible points
            for i, pnt in enumerate(self.unused):
                t1 = Triangle(self, pnt, edge.i1, edge.i2)      # create triangle with point & new edge
                # if new triangle is a valid addition, add a feasible point based on this pnt/edge combination
                if t1.a < float("inf") and (not t1.intersectPoly()) and t1.interiorEmpty(nonpoly):
                    self.feasible.append((pnt, edge, t1))
                    add.append(pnt)

            # remove/add elements to feasible/unused lists before moving to next new edge
            for i in add:
                self.unused.remove(i)
            for i in kill:
                self.feasible.remove(i)
                self.unused.append(i[0])
            add = []
            kill = []

        self.newedges = []  # empty newedges list

    # ---------------------------------------------------
    def randomFeasiblePoint(self):
        """
        This method chooses a randomly selected point that can be feasibly inserted into the current polygon
        A point p is feasible if:
          - no remaining points lie in the interior of the polygon when this point is added
          - adding this point does not cause the polygon to have self-intersections
        Output: updates self.feasible and self.unused, does not return any values
        """
        num = min(len(self.poly), 10)              # number of "closest" polygon vertices to look at
        notseen = [ind for ind in self.unused]     # unused points, so far unchecked as feasible point
        isValid = False
        while not isValid:
            if len(notseen) < 2:
                ii = 0
            else:
                ii = random.randint(0,len(notseen)-1)   # choose random point
            i = notseen[ii]     # index of candidate feasible point
            # find closest points in polygon
            p = [ind for ind in self.dsort[i][1:] if ind in self.poly]
            j = 0
            ct = 0
            t = []
            # loop through list of closest points until a valid connection is found
            while j < len(p) and ct < num:
                # if connecting to this point causes the polygon to self-intersect --> look at next-closest polygon vertex
                if Edge(self, p[j], i).intersectPoly():
                    j += 1
                # otherwise save the two triangles formed by connecting i to either of this polygon vertex's edges
                else:
                    ind = self.poly.index(p[j])
                    neighbor = [ind - 1, ind + 1]
                    if neighbor[1] >= len(self.poly):
                        neighbor[1] = 0
                    t.append(Triangle(self, i, self.poly[neighbor[0]], self.poly[ind]))
                    t.append(Triangle(self, i, self.poly[ind], self.poly[neighbor[1]]))
                    j += 1
                    ct += 1
            # if no triangles were saved and there are no other unused vertices to be checked,
            # break to avoid infinite loop. No feasible points have been found.
            if ct == 0 and len(notseen) == 1:
                break
            # sort triangles by minimal area
            t.sort(key=lambda x: x.a)
            # find smallest area triangle with no interior points
            points_temp = [pnt for pnt in self.unused]  # first copy list of unused points to check interior of triangle
            points_temp.remove(i)
            # loop through all triangles (smallest to largest); break as soon as a valid addition is found
            for triangle in t:
                if triangle.a < float("inf") and triangle.interiorEmpty(points_temp) and not triangle.intersectPoly():
                    self.feasible = [(triangle.i0, Edge(self, triangle.i1, triangle.i2), triangle)]
                    self.unused.remove(triangle.i0)
                    isValid = True
                    break
            del notseen[ii]

    # ---------------------------------------------------
    # @staticmethod
    def buildPolygonization(self, tritype, edgetype):
        """Construct a simple polygonization for a given set of points (using all points)"""
        if tritype == 'RT':
            self.randomTriangle()
        else:
            self.randomGreedyTriangle()
        while len(self.poly) < self.np:     # continue until all points have been used in the polygon
            if edgetype == 'GAr':           # greedily choose point to add based on minimum possible increase in area
                self.feasiblePoints()
                i, a = min(enumerate(self.feasible), key=lambda x: x[1][2].a)     # index of min area over all feasible points
            else:                           # randomly choose point to add
                self.randomFeasiblePoint()
                i = 0                       # index of random feasible point
            # insert selected feasible point into polygon, between vertices of edge that will be replaced
            try:
                newp = self.feasible.pop(i)             # index of last vertex of edge to replace
                self.poly.insert(self.poly.index(newp[1].i2), newp[0])   # insert into poly list
                self.newedges = [Edge(self, newp[1].i1, newp[0]), Edge(self, newp[0], newp[1].i2)]  # set new edges
                self.used[newp[0]] = 1                  # set index to used
                self.a += newp[2].a                     # increase area by area of newly added triangle
            except:
                newtry = self.copy()
                newtry.buildPolygonization(tritype, edgetype)
                self.poly = newtry.poly

    # ---------------------------------------------------
    @staticmethod
    def optimalPolygonization(points, N):
        """Calculates N iterations of each polygonization approach, returns minimum area over all iterations"""
        def minimalPolygonization(polygon, tritype, edgetype):
            polyN = [[] for i in range(N)]
            areaN = [-1 for i in range(N)]
            for i in xrange(N):
                try:
                    pcopy = polygon.copy()
                    pcopy.buildPolygonization(tritype, edgetype)
                    polyN[i] = [pcopy.points[x] for x in pcopy.poly]
                    areaN[i] = pcopy.a
                except:
                    areaN[i] = 699*699

            i, area = min(enumerate(areaN), key=lambda x: x[1])
            return polyN[i], area

        tritype = ['RGT', 'RT']
        # tritype = ['RGT']
        edgetype = ['RPGE', 'GAr']
        # edgetype = ['RPGE']
        nt = len(tritype)
        ne = len(edgetype)
        poly = [[] for i in range(nt * ne)]
        area = [699*699 for i in range(nt * ne)]
        polygon_blank = Polygon(points)
        # save optimal area from each algorithm
        for i in xrange(nt):
            for j in xrange(ne):
                k = (i - 1) * nt + j
                poly[k], area[k] = minimalPolygonization(polygon_blank, tritype[i], edgetype[j])

        i, a = min(enumerate(area), key=lambda x: x[i])
        return poly[i]