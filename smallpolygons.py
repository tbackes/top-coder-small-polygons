#!/usr/local/bin/python
# Code to interface with visualization/tester
import minareapolygon
import sys


"""
This code was constructed to solve a Topcoder Marathon Match problem statement ("Small Polygons").
See header of minareapolygonization.py for more details.
"""


def choosePolygons(points, N):
    """
    Given list of points, return up to N polygons with minimum total area.
    points is a list of Np*2 integers, corresponding to Np "points" (e.g. [x1, y1, x2, y2, ..., xNp, yNp])

    This code returns a list of polygons, where each polygon is a space delimited, ordered list of the polygon's
    vertices; Each vertex is identified by its index in the original 'points' list. (not by its explicit coordinate
    pair)
    """
    allpoints = [minareapolygon.Pnt(points[2 * i], points[2 * i + 1]) for i in range(len(points) / 2)]

    # change number of iterations to run based on number of points in space; values to use depend on run time limits
    Np = len(allpoints)
    if Np < 100:
        iterations = 20
    elif Np < 250:
        iterations = 5
    else:
        iterations = 1

    # assign points to N equally spaced vertical partitions based on y value. (so no intersecting polygons)
    SZ = 700        # size of grid, in both x and y directions
    breaks = [(SZ / N) * i for i in range(N)]
    breaks.append(SZ+1)
    pnts = [[pnt for pnt in allpoints if breaks[i] <= pnt.y < breaks[i + 1]] for i in range(N)]

    # make sure at least 3 points in each vertical slice:
    i = 1
    while len(min(pnts, key = lambda x: len(x))) < 3:
        # for i in xrange(1,len(pnts)):
        if len(pnts[i]) < 3:
            pnts[i-1] = pnts[i-1] + pnts.pop(i)
        i += 1
        if i >= len(pnts):
            i = 1

    # calculate approximate "optimal" polygonization for each partition/slice
    poly = []
    for i in range(len(pnts)):
        if len(pnts[i]) > 2:
            poly.append(minareapolygon.Polygon.optimalPolygonization(pnts[i], iterations))
    polyInd = [[allpoints.index(x) for x in pset] for pset in poly]
    return [' '.join(map(str,pi)) for pi in polyInd]


# --code to interface with Top Coder implementation (i.e. read in list of points/parameters) --------------------------
Np = int(sys.stdin.readline())
points = [0 for i in range(Np)]
for i in xrange(Np):
    points[i] = int(sys.stdin.readline())
N = int(sys.stdin.readline())

ret = choosePolygons(points, N)
print len(ret)
for item in ret:
    print item
sys.stdout.flush()

